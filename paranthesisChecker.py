STATE = False

def getInput():
    print("Input    :")
    inp=input()
    return inp

def stackPopper(stack, element):
    try:
        prevElement,elemPos = stack[len(stack)-1]
    except IndexError:
        return stack,True

    if element==")" and prevElement =="(" :
        stack.pop()
        return stack,False
    elif element=="]" and prevElement =="[" :
        stack.pop()
        return stack,False
    elif element=="}" and prevElement =="}" :
        stack.pop()
        return stack,False
    else:
        return stack,True

def listHandler(stack,currentElement,currentPos):
    if currentElement=="(" or currentElement =="[" or currentElement =="{":
        stack.append((currentElement,currentPos))
        return stack
    elif currentElement==")" or currentElement=="]" or currentElement=="}":
        stack,status = stackPopper(stack,currentElement)
        if status:
            global STATE
            STATE = True
            print("Output   :")
            print(currentPos+1)
            return
        else:
            return stack

def checkBal(inpString):
    currentPos=0
    count=0
    stack=list()
    for i in inpString:
        if i=="(" or i=="[" or i=="{":
            stack=listHandler(stack,i,currentPos)
            count=count+1
        if i==")" or i=="]" or i=="}":
            stack=listHandler(stack,i,currentPos)
        
        currentPos=currentPos+1

        if STATE:
            break
        #print(stack)
    print("Output   :")
    if not STATE and len(stack)==0:
        print("Success")
    elif not STATE:
        elem,elemPos=stack[0]
        print(elemPos+1)

inp=getInput()
checkBal(inp)
